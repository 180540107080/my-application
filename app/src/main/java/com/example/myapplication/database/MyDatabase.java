package com.example.myapplication.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {

    public static final String DATABASE_NAME = "MyApplication.db";
    public static final int DATABASE_VERSION = 2;

    public MyDatabase(Context context) {
        super(context,  DATABASE_NAME,  null, DATABASE_VERSION);
    }
}
