package com.example.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.jar.Attributes;

public class TblUserData extends MyDatabase{

    public static final String Tbl_USER_DATA = "Tbl_UserData";
    public static final String USER_ID = "UserID";
    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String EMAIL_ADDRESS = "EmailAddress";

    public TblUserData(Context context) {
        super(context);
    }

    public long insertUserDetail(String FirstName, String LastName, String PhoneNumber,String EmailAddress ){

        long insertedId = 0;
        if (isNumberAvailable(PhoneNumber)){
            insertedId = -1;
        }
        else{
            SQLiteDatabase db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(FIRST_NAME,FirstName);
            cv.put(LAST_NAME,LastName);
            cv.put(PHONE_NUMBER, PhoneNumber);
            cv.put(EMAIL_ADDRESS,EmailAddress);
            insertedId = db.insert(Tbl_USER_DATA,null,cv);
            db.close();
        }
        return insertedId;

    }
    public boolean isNumberAvailable(String PhoneNumber)
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM "+ Tbl_USER_DATA + " WHERE " + PHONE_NUMBER+"=?";
        Cursor cursor = db.rawQuery(query,new String[]{PhoneNumber});
        cursor.moveToFirst();
        boolean isNumberAvailable = cursor.getCount()>0;
        cursor.close();
        db.close();
        return isNumberAvailable;
    }
}
