package com.example.myapplication;

            import androidx.appcompat.app.AppCompatActivity;

            import android.content.Intent;
            import android.os.Bundle;
            import android.text.TextUtils;
            import android.view.View;
            import android.widget.Button;
            import android.widget.CheckBox;
            import android.widget.EditText;
            import android.widget.ImageView;
            import android.widget.RadioButton;
            import android.widget.RadioGroup;
            import android.widget.TextView;
            import android.widget.Toast;

            import com.example.myapplication.database.MyDatabase;
            import com.example.myapplication.database.TblUserData;
            import com.example.myapplication.util.Const;

            import java.util.ArrayList;
            import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName, etPhoneNumber, etEmailAddress;
    ImageView ivClose;
    Button btnSubmit;
    TextView tvDisplay;

    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbCricket;
    CheckBox chbHockey;
    CheckBox chbFootball;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        initViewReference();

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActlastName);
        etPhoneNumber = findViewById(R.id.etActPhoneNumber);
        etEmailAddress = findViewById(R.id.etActEmail);
        ivClose = findViewById(R.id.ivActClose);
        btnSubmit = findViewById(R.id.btnActSubmit);
        tvDisplay = findViewById(R.id.tvActDisplay);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {

                    String FirstName = etFirstName.getText().toString();
                    String LastName = etLastName.getText().toString();
                    String PhoneNumber = etPhoneNumber.getText().toString();
                    String EmailAddress = etEmailAddress.getText().toString();

                    TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long lastInsertedId = tblUserData.insertUserDetail(FirstName,LastName,PhoneNumber,EmailAddress);

                        Toast.makeText(getApplicationContext(),lastInsertedId > 0 ?
                                "User Inserted Successfully":  "Something went wrong",Toast.LENGTH_SHORT).show();








//                    HashMap<String, Object> map = new HashMap<>();
//                    map.put(Const.FIRST_NAME, etFirstName.getText().toString());
//                    map.put(Const.LAST_NAME, etLastName.getText().toString());
//                    map.put(Const.PHONE_NUMBER, etPhoneNumber.getText().toString());
//                    map.put(Const.EMAIL_ADDRESS, etEmailAddress.getText().toString());
//                    map.put(Const.GENDER, rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString());
//
//                    String hobbies = "";
//                    if (chbCricket.isChecked()) {
//                        hobbies += "" + chbCricket.getText().toString();
//                    }
//                    if (chbFootball.isChecked()) {
//                        hobbies += "" + chbFootball.getText().toString();
//                    }
//                    if (chbHockey.isChecked()) {
//                        hobbies += "" + chbFootball.getText().toString();
//                    }
//                    map.put(Const.HOBBY, hobbies);
//                    userList.add(map);



//                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
//                    intent.putExtra("UserList", userList);
//                    startActivity(intent);


                    /*EXPLISIT INTENT*/
//                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
//                String contactTempString = etFirstName.getText().toString() + "  " + etLastName.getText().toString() + " " + etPhoneNumber.getText().toString() + " " + etEmail.getText().toString();
//
//                intent.putExtra("contactTempString",contactTempString);
//                startActivity(intent);

//                Toast.makeText(getApplicationContext(), rbMale.isChecked()? "Male":"Female" ,Toast.LENGTH_LONG).show();


                }
            }
        });


        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootball.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);

                } else if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.GONE);
                    chbFootball.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    void initViewReference() {

        new MyDatabase(MainActivity.this).getReadableDatabase();

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbCricket = findViewById(R.id.chbActCricket);
        chbHockey = findViewById(R.id.chbActHockey);
        chbFootball = findViewById(R.id.chbActFootball);

    }

    boolean isValid() {
        boolean flag = true;
        if (TextUtils.isEmpty(etFirstName.getText())) {
            etFirstName.setError(getString(R.string.error_enter_value));
            flag = false;
        }
        if (TextUtils.isEmpty(etLastName.getText())) {
            etLastName.setError(getString(R.string.error_enter_value));
            flag = false;
        }
        if (TextUtils.isEmpty(etPhoneNumber.getText())) {
            etPhoneNumber.setError(getString(R.string.error_enter_value));
            flag = false;
        }else{
            String phoneNumber = etPhoneNumber.getText().toString();
            if (phoneNumber.length()<10){
                etPhoneNumber.setError("Enter Valid PhoneNumber:");
                flag = false;
            }
        }
        if (TextUtils.isEmpty(etEmailAddress.getText())) {
            etEmailAddress.setError(getString(R.string.error_enter_value));
            flag = false;
        }else{
            String email = etEmailAddress.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!email.matches(emailPattern)){
                etEmailAddress.setError("Enter Valid EmailAddress");
                flag = false;
            }
        }

        if(!(chbCricket.isChecked()&&chbCricket.isChecked()&&chbCricket.isChecked())){
            Toast.makeText(getBaseContext(),"Select any one check box",Toast.LENGTH_LONG).show();
            flag = false;
        }

        return flag;
    }
}
