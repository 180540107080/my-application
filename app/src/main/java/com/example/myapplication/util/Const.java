package com.example.myapplication.util;

public class Const {

    public  static  final String FIRST_NAME = "firstname";
    public  static  final String LAST_NAME = "lastname";
    public  static  final String PHONE_NUMBER = "phonenumber";
    public  static  final String EMAIL_ADDRESS = "emailaddress";
    public  static  final String GENDER = "gender";
    public  static  final String HOBBY = "hobby";

}
