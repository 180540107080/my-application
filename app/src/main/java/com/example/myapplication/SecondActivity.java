package com.example.myapplication;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class SecondActivity extends AppCompatActivity {

    ListView lvUsers;
    ArrayList<HashMap<String,Object>> userList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initViewReference();
        bindViewValues();
    }

    void bindViewValues() {
            userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        }


    void initViewReference(){

        lvUsers = findViewById(R.id.lvActUserList);
    }
}
